<h2>Система для проверки связности с удаленными устройствами</h2>

<h3>Команды для запуска сервиса:</h3>

sudo docker-compose up --build -d

<h3>Проверка состояния проекта:</h3>

docker logs project-test-connect

<h3>Внешний вид системы:</h3>

![Alt text](image-1.png)

<h3>Краткое описание:</h3>
Функционал системы позволяет добавлять устройства для проверки связности с ними.
К добавленным устрйоствам можно применить несколько действий.
<ul>
<li>Проверить - Выполняет проверку командой ping и traceroute;</li>
<li>Просмотр - Отображает лог запросов к серверу;</li>
<li>Удалить - Удаляет устройство из списка проверяемых.</li>
</ul>

Была переработана база данных. Оснавная таблица разделена на две.
Это опрашиваемые устройства и таблица проверок для каждого устройства.



