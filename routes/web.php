<?php

use Illuminate\Support\Facades\Route;


//Route::get('/', 'MainController@home')->name('home');
Route::get('/', 'App\Http\Controllers\MainController@home')->name('home');
Route::any('/add', 'App\Http\Controllers\MainController@addNewNode');
Route::get('/check/{id}', 'App\Http\Controllers\MainController@check');
Route::get('/view/{id}', 'App\Http\Controllers\MainController@view');
Route::get('/delete/{id}', 'App\Http\Controllers\MainController@delete');
Route::get('/deleteCheck/{node}/{id}', 'App\Http\Controllers\MainController@deleteCheck');
Route::any('/checkall', 'App\Http\Controllers\MainController@checkAll');
