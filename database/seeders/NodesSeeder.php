<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('nodes')->insert([
            ['name' => 'yandex.ru', 'ip' => '77.88.55.88'],
            ['name' => 'google.com', 'ip' => '64.233.163.139'],
            ['name' => 'vk.com', 'ip' => '87.240.132.78'],
            ['name' => 'thingiverse.com', 'ip' => '104.19.147.91'],
            ['name' => 'stackoverflow.com', 'ip' => '188.114.99.224'],
            ['name' => 'chess.com', 'ip' => '34.117.44.137'],
        ]);
    }
}
