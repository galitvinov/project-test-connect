<?php

namespace App\Http\Controllers;

use App\Models\Checks;
use App\Models\Nodes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\New_;

class MainController extends Controller
{
    public function home(){
        $list = DB::table('nodes')->get();
        return view('home', compact('list'));
    }

    public function addNewNode(Request $request){

        if($request->method() == 'POST'){
            $node = New Nodes;
            $node->name = $request['name'];
            $node->ip = $request['ip'];
            $node->save();
            return redirect('/');
        }
        return view('add');
    }

    public function check($id){
        $count = 1;
        $node = Nodes::find($id);
        $time = Checks::checkPing($node, $count);
        $nodesCount = Checks::checkTraceroute($node);
        $check = New Checks;
        $check->node_id = $node->id;
        $check->ping = $time;
        $check->nodes = $nodesCount;
        $check->save();
        return redirect('/view/' . $node->id);
    }

    public function checkAll(){
        $list = DB::table('nodes')->get();
        foreach ($list as $element){
            $count = 1;
            $time = Checks::checkPing($element, $count);
            $nodesCount = Checks::checkTraceroute($element);
            $check = New Checks;
            $check->node_id = $element->id;
            $check->ping = $time;
            $check->nodes = $nodesCount;
            $check->save();
        }
        return redirect('/');
    }

    public function view($id){
        $node = Nodes::find($id);
        $checks = DB::table('checks')->where('node_id', $node->id)->orderByDesc('id')->get();
        return view('view', compact('node','checks'));
    }

    public function delete($id){
        DB::table('nodes')->where('id', $id)->delete();
        DB::table('checks')->where('node_id', $id)->delete();
        return redirect('/');
    }

    public function deleteCheck($node, $id){
        DB::table('checks')->where('id', $id)->delete();
        return redirect('/view/' . $node);
    }
}
