<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $created_at
 * @property int $updated_at
 * @property int $node_id
 * @property int $ping
 * @property int $nodes
 */
class Checks extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'checks';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_at', 'updated_at', 'node_id', 'ping', 'nodes'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'timestamp', 'updated_at' => 'timestamp', 'node_id' => 'int', 'ping' => 'int', 'nodes' => 'int'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...
    public static function checkPing($node, $count = 1){
        exec("ping -c " . $count . " " . $node->ip, $output, $result);
        if (count($output) <= 4){
		return 9999;
        }
        $time = explode("=", $output[$count+4])[1];
        $time = explode("/", $time)[1];
        $time = intval($time);
        return $time;
    }

    public static function checkTraceroute($node){
        exec("traceroute -I --wait=2 --tries=1 " . $node->ip, $output, $result);
        return max((count($output) - 1), 0);
    }
    // Relations ...
}
