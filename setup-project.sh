#!/bin/bash

directory="vendor"
if [ ! -d "$directory" ]; then
    composer update
    php artisan migrate
    php artisan db:seed --class=NodesSeeder
fi
