FROM php:8.1.3RC1-apache
RUN apt-get update && apt-get install -y nano mc curl iputils-ping inetutils-traceroute
RUN curl -sS https://getcomposer.org/installer | php \
  && chmod +x composer.phar && mv composer.phar /usr/local/bin/composer

RUN docker-php-ext-install pdo_mysql

ENV TZ=Asia/Omsk
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN printf '[PHP]\ndate.timezone = "Asia/Omsk"\n' > /usr/local/etc/php/conf.d/tzone.ini

WORKDIR /app
CMD ./setup-project.sh && php artisan serve --host=0.0.0.0 --port=80
