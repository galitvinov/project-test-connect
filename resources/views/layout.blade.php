<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('css/layout.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('css/loading-style.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('js/bootstrap.bundle.min.js') }}" rel="script">

    <title>{{config('app.name','APP')}} -@yield('title')</title>
</head>
<body>
<div class="container col-xl-10 col-xxl-8 px-4 py-5">
<section class="sec-loading invisible" id="mainbox" style="position: fixed;
        right: 0px;
        width:100%;
        height:100%;
        z-index:99999999; 
        background-color: rgba(0, 0, 0, 0.3);">
<div class="one">
</div>
</section>
    <div class="align-items-center g-lg-5">
        @yield('content')
    </div>
</div>
</body>
</html>
