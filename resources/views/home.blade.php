@extends('layout')

@section('title')
    Home
@endsection

@section('content')
    <div class="row">
        <center><h1>Центр управления запросами</h1></center>
        <div class="row-cols-md-2">
            <a href="/add"><button class="btn btn-primary">Добавление нового устройства</button></a>
            <a href="/checkall"><button onclick="document.getElementById('mainbox').className = 'sec-loading'" class="btn btn-warning">Проверка всех устройств</button></a>
        </div>
        <h1 class="display-6 fw-bold lh-1 mb-3">Список проверяемых устройств</h1>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Название</th>
                <th scope="col">IP-адрес</th>
                <th scope="col">Последняя проверка</th>
                <th scope="col" width="35%">Действия</th>
            </tr>
            </thead>
            <tbody>
            @if($list->count())
            @foreach ($list as $element)
                <tr>
                    <th scope="row">{{ $element->id }}</th>
                    <td>{{ $element->name }}</td>
                    <td>{{ $element->ip }}</td>
                    <td>{{ \App\Models\Nodes::lastCheck($element->id) }}</td>
                    <td>
                        <a href="/check/{{  $element->id  }}" style="text-decoration: none;">
                            <button onclick="document.getElementById('mainbox').className = 'sec-loading'" type="button" class="btn btn-sm btn-success">Проверить</button>
                        </a>
                        <a href="/view/{{  $element->id  }}" style="text-decoration: none;">
                            <button type="button" class="btn btn-sm btn-secondary">Просмотр</button>
                        </a>
                        <a href="/delete/{{  $element->id  }}" style="text-decoration: none;">
                            <button type="button" class="btn btn-sm btn-danger">Удалить</button>
                        </a>
                    </td>
                </tr>
            @endforeach
            @else
                <tr><th colspan="4"><center>Устройства отсутствуют</center></th></tr>
            @endif
            </tbody>
        </table>
    </div>
@endsection
