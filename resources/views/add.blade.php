@extends('layout')

@section('title')
    Add
@endsection

@section('content')
    <div class="row">
        <a href="/">Вернуться к списку устройств</a>
        <center><h1>Добавление нового устройства</h1></center>
        <form method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label for="name" class="iconic" > Название <span class="required">*</span></label>
                <input type="text" class="form-control" name="name" id="name"  required="required" placeholder="Название">
            </div><br />
            <div class="form-group">
                <label for="ip" class="iconic mail-alt"> IP-адрес <span class="required">*</span></label>
                <input type="text" class="form-control" name="ip" id="ip" placeholder="IP-адрес" required="required">
            </div>
            <br />
            <input type="submit" class="btn btn-success" value="Добавить" />
        </form>
    </div>
@endsection
