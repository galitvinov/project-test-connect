@extends('layout')

@section('title')
    View
@endsection

@section('content')
    <div class="row">
        <a href="/">Вернуться к списку устройств</a><br /><br />
        <div class="bg-light p-5 rounded">
            <div class="col-md-10 mx-auto col-lg-10">
                <center><h1>Проверки для записи {{ $node->id }}</h1></center>
                <div class="row"><h5 style="width: 20%">Название:</h5> {{ $node->name }}</div>
                <div class="row"><h5 style="width: 20%">IP-адрес:</h5> {{ $node->ip }}</div>
            </div>
        </div>
        <br />
        <div>
            <a href="/check/{{  $node->id  }}"><button  onclick="document.getElementById('mainbox').className = 'sec-loading'" class="btn btn-primary my-2">Выполнить проверку</button></a>
        </div>
        <h3 >Список выполненных проверок</h3>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Время рing</th>
                <th scope="col">Кол-во переходов traceroute</th>
                <th scope="col" width="35%">Действия</th>
            </tr>
            </thead>
            <tbody>
            @if($checks->count())
                <?php $number = 1; ?>
                @foreach ($checks as $element)
                    <tr>
                        <th scope="row">{{ $number++ }}</th>
                        <td>{{ $element->ping }}</td>
                        <td>{{ $element->nodes }}</td>
                        <td>
                            <a href="/deleteCheck/{{ $node->id }}/{{  $element->id  }}" style="text-decoration: none;">
                                <button type="button" class="btn btn-sm btn-danger">Удалить</button>
                            </a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr><th colspan="4"><center>Записи отсутствуют</center></th></tr>
            @endif
            </tbody>
        </table>
    </div>
@endsection
